from django.test import TestCase
from django.urls import include, reverse
from api.models import CSCI, CSU, File, Test, TestRun, TestResult
from rest_framework import status
from rest_framework.test import APIRequestFactory, APITestCase


class CreateObjectsTestCase(TestCase):

    def populate_db(self):
        csci = CSCI.objects.create(name='csci1')
        CSCI.objects.create(name='csci2')
        csu1 = CSU.objects.create(name='csu1', csci=csci)
        csu2 = CSU.objects.create(name='csu2', csci=csci)
        csu3 = CSU.objects.create(name='csu3', csci=csci)

        file1 = File.objects.create(name='fil1')
        file1.csus.add(csu1,csu2 )
        file1.save()

        file2 = File.objects.create(name='fil2')
        file2.csus.add(csu2, csu3 )
        file2.save()

        test_run = TestRun.objects.create(name='testkörning', timestamp='2015-10-22T19:50:08Z', computer='dator1',
                                          label='trail', HLR_entry=55.55, statement_coverage=55.55, decision_coverage=55.55, MC_DC_coverage=55.55)

        test_result1 = TestResult.objects.create(file=file1 , test_run=test_run, HLR_entry=55.55,
                                                statement_coverage=55.55, decision_coverage=55.55,MC_DC_coverage= 55.55 )

        test_result2 = TestResult.objects.create(file=file2 , test_run=test_run, HLR_entry=77.55,
                                                statement_coverage=44.55, decision_coverage=22.55,MC_DC_coverage= 11.55 )
        Test.objects.create(name= 'test1', test_run = test_run, passed = 3, failed = 1)
        Test.objects.create(name= 'test2', test_run = test_run, passed = 5, failed = 2)
        Test.objects.create(name= 'test3', test_run = test_run, passed = 6, failed = 1)


    def test_object_lookup(self):
        self.populate_db()
        csci = CSCI.objects.get(name='csci1')
        self.assertEqual(csci.name , 'csci1')

        csu = CSU.objects.get(name='csu1')
        self.assertEqual(csu.name , 'csu1')

        file = File.objects.get(name='fil1')
        self.assertEqual(file.name , 'fil1')

        test_run = TestRun.objects.get(name='testkörning')
        self.assertEqual(test_run.name , 'testkörning')

        test_result1 = TestResult.objects.get(file__name='fil1')
        self.assertEqual(test_result1.file.name , 'fil1')

        test_result2 = TestResult.objects.get(file__name='fil2')
        self.assertEqual(test_result2.file.name , 'fil2')


class APITests(APITestCase):

    def populate_db(self):
        csci = CSCI.objects.create(name='csci1')
        CSCI.objects.create(name='csci2')
        csu1 = CSU.objects.create(name='csu1', csci=csci)
        csu2 = CSU.objects.create(name='csu2', csci=csci)
        csu3 = CSU.objects.create(name='csu3', csci=csci)

        file1 = File.objects.create(name='fil1')
        file1.csus.add(csu1,csu2 )
        file1.save()

        file2 = File.objects.create(name='fil2')
        file2.csus.add(csu2, csu3 )
        file2.save()

        test_run = TestRun.objects.create(name='testkörning', timestamp='2015-10-22T19:50:08Z', computer='dator1',
                                          label='trail', HLR_entry=55.55, statement_coverage=55.55, decision_coverage=55.55, MC_DC_coverage=55.55)

        test_result1 = TestResult.objects.create(file=file1 , test_run=test_run, HLR_entry=88.55,
                                                statement_coverage=88.55, decision_coverage=88.55,MC_DC_coverage= 88.55 )

        test_result2 = TestResult.objects.create(file=file2 , test_run=test_run, HLR_entry=77.55,
                                                statement_coverage=77.55, decision_coverage=77.55,MC_DC_coverage= 77.55 )
        Test.objects.create(name= 'test1', test_run = test_run, passed = 3, failed = 1)
        Test.objects.create(name= 'test2', test_run = test_run, passed = 5, failed = 2)
        Test.objects.create(name= 'test3', test_run = test_run, passed = 6, failed = 1)

    def test_get_objects(self):
        
        self.populate_db()

        url = reverse('api:csci-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:csu-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:file-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:test_run-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:test_result-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:test-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_specific_object(self):
        self.populate_db()

        cscis = CSCI.objects.values()
        url = reverse('api:csci-detail', args=[cscis[0]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.data['name'] , cscis[0]['name'])

        url = reverse('api:csci-detail', args=[cscis[1]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.data['name'] , cscis[1]['name'])

        url = reverse('api:csci-detail', args=[cscis[1]['id'] + 1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        csus = CSU.objects.values()

        url = reverse('api:csu-detail', args=[csus[0]['id']])

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:csu-detail', args=[csus[1]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:csu-detail', args=[csus[2]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:csu-detail', args=[csus[2]['id'] + 1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        files = File.objects.values()
        url = reverse('api:file-detail', args=[files[0]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:file-detail', args=[files[1]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:file-detail', args=[files[1]['id'] + 1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        test_runs = TestRun.objects.values()
        url = reverse('api:test_run-detail', args=[test_runs[0]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:test_run-detail', args=[test_runs[0]['id'] + 1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        test_results = TestResult.objects.values()
        url = reverse('api:test_result-detail', args=[test_results[0]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        tests = Test.objects.values()
        url = reverse('api:test-detail', args=[tests[0]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:test-detail', args=[tests[1]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:test-detail', args=[tests[2]['id']])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('api:test-detail', args=[tests[2]['id'] + 1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


    def test_post_objects(self):

        url = reverse('api:csci-list')
        data = [{'name':'csci1'},{'name':'csci2'},{'name':'csci3'}, ]
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(CSCI.objects.count(), 3)
  
        url = reverse('api:csu-list')
        data = [{ 'name' : 'csu1', 'csci': CSCI.objects.values()[0]['id'] }, { 'name' : 'csu2', 'csci': CSCI.objects.values()[0]['id'] }, { 'name' : 'csu3', 'csci': CSCI.objects.values()[0]['id'] }]
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(CSU.objects.count(), 3)

        url = reverse('api:file-list')
        data = [{  'name': 'fil1', 'csus': [CSU.objects.values('pk')[0]['pk'], 
        CSU.objects.values('pk')[1]['pk'], CSU.objects.values('pk')[2]['pk']]} ]
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(File.objects.count(), 1)
        self.assertEqual(File.objects.get().name, 'fil1')

        TestRun.objects.create(name='testkörning', timestamp='2015-10-22T19:50:08Z', computer='dator1',
                                          label='trail', HLR_entry=55.55, statement_coverage=55.55, decision_coverage=55.55, MC_DC_coverage=55.55)

        url = reverse('api:test_result-list')
        data = [{'file': File.objects.values('pk')[0]['pk'],'test_run': TestRun.objects.values('pk')[0]['pk'], 'HLR_entry':55.55, 
        'statement_coverage':55.55, 'decision_coverage':55.55, 'MC_DC_coverage': 55.55 }]
        response = self.client.post(url, data, format='json')

        self.assertEqual(TestResult.objects.count(), 1)

        url = reverse('api:test-list')

        data = [{ 'name' : 'test1', 'test_run' : TestRun.objects.values('pk')[0]['pk'], 'passed': 5, 'failed': 3 },
                { 'name' : 'test2', 'test_run' : TestRun.objects.values('pk')[0]['pk'], 'passed': 8, 'failed': 2 } ,
                { 'name' : 'test3', 'test_run' : TestRun.objects.values('pk')[0]['pk'], 'passed': 7, 'failed': 33 } ]
        response = self.client.post(url, data, format='json')

        self.assertEqual(Test.objects.count(), 3)

    def test_patch_API(self):

        self.populate_db()

        object_id = TestRun.objects.values()[0]["id"]
        url = reverse('api:test_run-detail', args=[object_id])
        data = {'name':'patched_name'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(TestRun.objects.get(id=object_id).name, "patched_name")

        url = reverse('api:test_run-detail', args=[object_id])
        data = {'label':'peer'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(TestRun.objects.get(id=object_id).label, "peer")

        url = reverse('api:test_run-detail', args=[object_id])
        data = {'label':'olagligt'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_search_API(self):

        self.populate_db()

        url = reverse('api:csci-list') + '?name=csci1'
        response = self.client.get(url, format='json')

        self.assertEqual(response.data[0]["name"] , "csci1" )

        url = reverse('api:csci-list') + '?name=easdasd'
        response = self.client.get(url, format='json')
        self.assertEqual(response.json() , [])

        object_id = CSCI.objects.values()[0]["id"]
        url = reverse('api:file-list') + '?csci=' + str(object_id)
        response = self.client.get(url, format='json')
        self.assertEqual(len(response.json()), 2)

        object_id = CSCI.objects.values()[1]["id"]
        url = reverse('api:file-list') + '?csci=' + str(object_id)
        response = self.client.get(url, format='json')
        self.assertEqual(len(response.json()), 0)

        test_run2 = TestRun.objects.create(name='testkörning2', timestamp='2017-11-22T19:50:08Z', computer='dator1',
                               label='trail', HLR_entry=44.55, statement_coverage=33.55, decision_coverage=22.55, MC_DC_coverage=11.55)
        
        test_run3 = TestRun.objects.create(name='testkörning3', timestamp='2019-12-22T19:50:08Z', computer='dator1',
                               label='trail', HLR_entry=77.55, statement_coverage=22.55, decision_coverage=11.55, MC_DC_coverage=99.55)

        object_id = CSCI.objects.values()[0]['id']
        url = reverse('api:test_run-list') + '?csci=' + str(object_id)
        response = self.client.get(url, format='json')
        self.assertEqual(response.json()[0]['name'], 'testkörning')

        url = reverse('api:test_run-list') + '?recent=2'
        response = self.client.get(url, format='json')
        self.assertEqual(len(response.json()), 2)

        url = reverse('api:test_run-list') + '?recent=20'
        response = self.client.get(url, format='json')
        self.assertEqual(len(response.json()), 3)
 
        url = reverse('api:test_run-list') + '?timestamp=2015-10-22'
        response = self.client.get(url, format='json')
        self.assertEqual(response.json()[0]["timestamp"], '2015-10-22T19:50:08Z')

        url = reverse('api:test_run-list') + '?timestamp=2017-11-22'
        response = self.client.get(url, format='json')
        self.assertEqual(response.json()[0]["timestamp"], '2017-11-22T19:50:08Z')

        url = reverse('api:test_run-list') + '?timestamp=2020-12-30'
        response = self.client.get(url, format='json')
        self.assertEqual(response.json(), [])


        csci = CSCI.objects.get(name="csci2")
        csu4 = CSU.objects.create(name='csu4', csci=csci)
        file3 = File.objects.create(name='fil3')
        file3.csus.add(csu4)
        file3.save()
        TestResult.objects.create(file=file3 , test_run=test_run2, HLR_entry=33.55,
                                                    statement_coverage=33.55, decision_coverage=33.55,MC_DC_coverage= 33.55 )
                                            

        url = reverse('api:csci-list') + '?test_run=' + str(TestRun.objects.get(name='testkörning').id)
        response = self.client.get(url, format='json')
        self.assertEqual(response.json()[0]["name"], "csci1")
        self.assertEqual(len(response.json()), 1)

        url = reverse('api:csci-list') + '?test_run=33'
        response = self.client.get(url, format='json')
        self.assertEqual(response.json(), [])

        url = reverse('api:csci-list') + '?test_run=' + str(TestRun.objects.get(name='testkörning').id) + '&test_run=' + str(TestRun.objects.get(name='testkörning2').id)
        response = self.client.get(url, format='json')
        self.assertEqual(response.json()[0]["name"], "csci1")
        self.assertEqual(response.json()[1]["name"], "csci2")
        self.assertEqual(len(response.json()), 2)

        url = reverse('api:csci-list') + '?test_run=' + str(TestRun.objects.get(name='testkörning2').id) + '&test_run=' + str(TestRun.objects.get(name='testkörning3').id)
        response = self.client.get(url, format='json')
        self.assertEqual(response.json()[0]["name"], "csci2")
        self.assertEqual(len(response.json()), 1)

        csci = CSCI.objects.get(name="csci1")
        csu1 = CSU.objects.get(name='csu1', csci=csci)
        file3.csus.add(csu1)
        file3.save()

        url = reverse('api:csci-list') + '?test_run=' + str(TestRun.objects.get(name='testkörning').id) 
        response = self.client.get(url, format='json')
        self.assertEqual(response.json()[0]["HLR_entry"], '83.05')
        self.assertEqual(response.json()[0]["statement_coverage"], '83.05')
        self.assertEqual(response.json()[0]["decision_coverage"], '83.05')
        self.assertEqual(response.json()[0]["MC_DC_coverage"], '83.05')

        url = reverse('api:csci-list') + '?test_run=' + str(TestRun.objects.get(name='testkörning').id) + '&test_run=' + str(TestRun.objects.get(name='testkörning2').id)
        response = self.client.get(url, format='json')  
        self.assertEqual(response.json()[0]["HLR_entry"], '66.55')
        self.assertEqual(response.json()[0]["statement_coverage"], '66.55')
        self.assertEqual(response.json()[0]["decision_coverage"], '66.55')
        self.assertEqual(response.json()[0]["MC_DC_coverage"], '66.55')


        url = reverse('api:csu-list') + '?test_run=' + str(TestRun.objects.get(name='testkörning').id) 
        response = self.client.get(url, format='json')  
        self.assertEqual(response.json()[0]["HLR_entry"], '88.55')
        self.assertEqual(response.json()[0]["statement_coverage"], '88.55')
        self.assertEqual(response.json()[0]["decision_coverage"], '88.55')
        self.assertEqual(response.json()[0]["MC_DC_coverage"], '88.55')

        url = reverse('api:csu-list') + '?test_run=' + str(TestRun.objects.get(name='testkörning').id) + '&test_run=' + str(TestRun.objects.get(name='testkörning2').id)
        response = self.client.get(url, format='json')  
        self.assertEqual(response.json()[0]["HLR_entry"], '61.05')
        self.assertEqual(response.json()[0]["statement_coverage"], '61.05')
        self.assertEqual(response.json()[0]["decision_coverage"], '61.05')
        self.assertEqual(response.json()[0]["MC_DC_coverage"],'61.05')

        TestResult.objects.create(file=file3 , test_run=test_run3, HLR_entry=44.55,
                                                    statement_coverage=44.55, decision_coverage=44.55,MC_DC_coverage= 44.55 )

        url = reverse('api:file-list') + '?test_run=' + str(TestRun.objects.get(name='testkörning2').id) 
        response = self.client.get(url, format='json')
        self.assertEqual(response.json()[0]["HLR_entry"], '33.55')
        self.assertEqual(response.json()[0]["statement_coverage"], '33.55')
        self.assertEqual(response.json()[0]["decision_coverage"], '33.55')
        self.assertEqual(response.json()[0]["MC_DC_coverage"], '33.55')

        url = reverse('api:file-list') + '?test_run=' + str(TestRun.objects.get(name='testkörning2').id) + '&test_run=' + str(TestRun.objects.get(name='testkörning3').id)
        response = self.client.get(url, format='json')  
        self.assertEqual(response.json()[0]["HLR_entry"], '39.05')
        self.assertEqual(response.json()[0]["statement_coverage"], '39.05')
        self.assertEqual(response.json()[0]["decision_coverage"], '39.05')
        self.assertEqual(response.json()[0]["MC_DC_coverage"], '39.05')


    def test_delete(self):

        #Delete cascade Test run -> CSCI
        self.populate_db()

        object_id = TestRun.objects.values()[0]["id"]
        url = reverse('api:test_run-detail', args=[object_id])
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)  
        self.assertEqual(len(TestRun.objects.all()), 0)
        self.assertEqual(len(TestResult.objects.all()), 0)
        self.assertEqual(len(Test.objects.all()), 0)
        self.assertEqual(len(File.objects.all()), 0)
        self.assertEqual(len(CSU.objects.all()), 0)
        self.assertEqual(len(CSCI.objects.all()), 1)

        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        object_id = CSCI.objects.values()[0]["id"]
        url = reverse('api:csci-detail', args=[object_id])
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(CSCI.objects.all()), 0) 

        #Delete only the old Test run and its connected objects
        self.populate_db()
        object_id = TestRun.objects.values()[0]["id"]
        test_run = TestRun.objects.create(name='testkörning2', timestamp='2018-12-22T19:50:08Z', computer='dator1',
                                          label='trail', HLR_entry=55.55, statement_coverage=55.55, decision_coverage=55.55, MC_DC_coverage=55.55)
        test_result1 = TestResult.objects.create(file=File.objects.get(name="fil1") , test_run=test_run, HLR_entry=55.55,
                                                statement_coverage=55.55, decision_coverage=55.55,MC_DC_coverage= 55.55 )
 
        url = reverse('api:test_run-detail', args=[object_id])
        response = self.client.delete(url, format='json')
        self.assertEqual(len(TestRun.objects.all()), 1)
        self.assertEqual(len(TestResult.objects.all()), 1)
        self.assertEqual(len(Test.objects.all()), 0)
        self.assertEqual(len(File.objects.all()), 1)
        self.assertEqual(len(CSU.objects.all()), 2)
        self.assertEqual(len(CSCI.objects.all()), 2)

        #Delete Files and its connected CSU if no other files are connected to that CSU
        csci = CSCI.objects.get(name="csci1")
        csu4 = CSU.objects.create(name='csu4', csci=csci)
        csu5 = CSU.objects.create(name='csu5', csci=csci)
        csu6 = CSU.objects.create(name='csu6', csci=csci)
        csu7 = CSU.objects.create(name='csu7', csci=csci)

        file3 = File.objects.create(name='fil3')
        file3.csus.add(csu4,csu5, csu6 )
        file3.save()

        file4 = File.objects.create(name='fil4')
        file4.csus.add(csu5, csu7)
        file4.save()

        file5 = File.objects.create(name='fil3')
        file5.csus.add(csu4,csu6, csu7)
        file5.save()

        url = reverse('api:file-detail', args=[file3.id])
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(File.objects.filter(name="fil4")), 1)
        self.assertEqual(len(CSU.objects.filter(name="csu4")), 1)
        self.assertEqual(len(CSU.objects.filter(name="csu5")), 1)
        self.assertEqual(len(CSU.objects.filter(name="csu6")), 1)
        self.assertEqual(len(CSU.objects.filter(name="csu7")), 1)

        url = reverse('api:file-detail', args=[file4.id])
        response = self.client.delete(url, format='json') 
        self.assertEqual(len(CSU.objects.filter(name="csu4")), 1)
        self.assertEqual(len(CSU.objects.filter(name="csu5")), 0)
        self.assertEqual(len(CSU.objects.filter(name="csu6")), 1)
        self.assertEqual(len(CSU.objects.filter(name="csu7")), 1)   

        url = reverse('api:file-detail', args=[file5.id])
        response = self.client.delete(url, format='json') 
        self.assertEqual(len(CSU.objects.filter(name="csu4")), 0)
        self.assertEqual(len(CSU.objects.filter(name="csu5")), 0)
        self.assertEqual(len(CSU.objects.filter(name="csu6")), 0)
        self.assertEqual(len(CSU.objects.filter(name="csu7")), 0)   
        
 

   

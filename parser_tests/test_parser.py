from collections import namedtuple
from pathlib import Path
from tcv import parse, match_file_with_csu_id, match_test_result_with_file_id, create_payloads_without_matched_id
from parse import parse_CSCI, parse_summary, parse_results, parse_config, parse_runner, parse_timestamp
from datetime import *
from classes import File, CSCI, Summary, Result


##################################
####     COMBINED TESTING     ####
##################################

def test_parse():
    args = namedtuple("Namespace", ["CSCI", "CSCI_name", "command", "config", "folder", "label", "name", "summary"])

    config = parse_config(Path(r"../input/test/db_con_test.toml").resolve())
    answer = (config,
              parse_CSCI(Path(r"../input/test/CSCI_test.ctr").resolve(), "My CSCI", config["CSCI_tree_path"]),
              parse_summary(Path(r"../input/test/summary.covsum").resolve()),
              parse_results([Path(r"../input/test/saab_test_result.ctr").resolve(),
                             Path(r"../input/test/failed_str.ctr").resolve(),
                             Path(r"../input/test/csci_test_integration.ctr").resolve(),
                             Path(r"../input/test/csci_test_regression.ctr").resolve(),
                             Path(r"../input/test/unit_test.ctr").resolve()
                             ])
              )
    res = parse(args(CSCI="CSCI_test.ctr",
                     CSCI_name="My CSCI",
                     command="parse",
                     config=r"../input/test/db_con_test.toml",
                     folder=r"../input/test/",
                     label="trail",
                     name="TEST_RUN",
                     summary="summary.covsum"
                     )
                )

    assert set(answer[3]) == set(res[3])
    assert answer[:-1] == res[:-1]


##############################
####     CSCI TESTING     ####
##############################

def test_parse_CSCI_with_tree():
    answer = CSCI(
        "TEST_NAME",
        [
            File("ls_rum_core.c", 100.0, 99.9, 99.8, 100.0, ["core"]),
            File("ls_rum_cache_invalidation_callback.c", 100.0, 100.0, 100.0,100.0, ["cache_invalidation_callback"]),
            File("ls_rum_cache_invalidation_request.c", 100.0, 100.0, 100.0, 100.0, ["cache_invalidation_request"])
         ],
        datetime(2020, 10, 30, 17, 3, 8),
        "jenkepic"
    )
    res = parse_CSCI(Path(r"../input/rum_core/CSCI.ctr").resolve(),
                     CSCI_name="TEST_NAME",
                     CSCI_tree_path=Path(r"../input/rum_core/csu.xml").resolve())
    assert res == answer


def test_parse_CSCI():
    answer = CSCI(
        "General",
        [
            File("ls_time_conversion.cpp", 46.7, 50.6, 48.3, 46.1),
            File("c_source_file.c", 20.0, 30.0, 40.0, 50.0),
            File("very_long_and_most_of_all_complicated_filename_that_should_not_be_long_in_the_first_place.cpp",
                 40.0, 50.0, 60.0, 15.0),
            File("very_long_and_most_of_all_complicated_filename_that_should_not_be_long_in_the_first_place.c",
                 45.0, 30.0, 40.0, 50.0)
        ],
        datetime(2018, 5, 30, 15, 28, 13),
        "hudsonavi"
    )
    res = parse_CSCI(Path(r"../input/ls_time_conversion/CSCI_test.ctr").resolve())
    assert res == answer


def test_long_CSCI_file():
    answer = CSCI(
        "General",
        [
            File("ls_time_conversion.cpp", 46.7, 50.6, 48.3, 46.1),
            File("c_source_file.c", 20.0, 30.0, 40.0, 50.0),
            File("very_long_and_most_of_all_complicated_filename_that_should_not_be_long_in_the_first_place.cpp",
                 40.0, 50.0, 60.0, 15.0),
            File("very_long_and_most_of_all_complicated_filename_that_should_not_be_long_in_the_first_place.c",
                 45.0, 30.0, 40.0, 50.0)
        ],
        datetime(2018, 5, 30, 15, 28, 13),
        "hudsonavi"
    )
    res = parse_CSCI(Path("../input/ls_time_conversion/CSCI_test_long.ctr").resolve(), "General")
    assert res == answer


def test_get_timestamp():
    answer = datetime(2018, 5, 30, 15, 28, 13)
    res = parse_timestamp(Path("../input/ls_time_conversion/CSCI_test.ctr").resolve())
    assert res == answer


def test_get_runner():
    answer = "hudsonavi"
    res = parse_runner(Path(r"../input/ls_time_conversion/CSCI_test.ctr").resolve())
    assert res == answer


##############################
####    SUMMARY TESTING   ####
##############################

def test_parse_summary():
    answer = Summary(41.7, 36.2, 21.0, 5.7)
    res = parse_summary(Path(r"../input/test/summary.covsum").resolve())
    assert res == answer


def test_parse_summary_na():
    answer = Summary(None, 36.2, None, 5.7)
    res = parse_summary(Path(r"../input/test/summary_na.covsum").resolve())
    assert res == answer


##############################
####    RESULT TESTING    ####
##############################

def test_result_with_list():
    answer = [
        Result("VC-LS_Time_Conversion-550", 5, 1),
        Result("VC-LS_Time_Conversion-548", 65, 3),
        Result("VC-LS_Time_Conversion-869", 3, 3),
        Result("VC-LS_Time_Conversion-842", 18, 2),
        Result("VC-LS_Time_Conversion-870", 57, 7),
        Result("VC-LS_Time_Conversion-872", 29, 5),
        Result("VC-LS_Time_Conversion-873", 68, 8)
    ]
    res = parse_results([Path(r"../input/ls_time_conversion/saab_test_result.ctr").resolve(),
                         Path(r"../input/ls_time_conversion/failed_str.ctr").resolve()])


def test_parse_result():
    answer = [
        Result("VC-LS_Time_Conversion-550", 3, 0),
        Result("VC-LS_Time_Conversion-548", 34, 0),
        Result("VC-LS_Time_Conversion-869", 3, 0),
        Result("VC-LS_Time_Conversion-842", 10, 0),
        Result("VC-LS_Time_Conversion-870", 32, 0),
        Result("VC-LS_Time_Conversion-872", 17, 0),
        Result("VC-LS_Time_Conversion-873", 38, 0)
    ]
    res = parse_results([Path(r"../input/ls_time_conversion/saab_test_result.ctr").resolve()])
    assert res == answer


def test_long_result_file():
    answer = [
        Result("VC-LS_Time_Conversion-550", 157464, 0),
        Result("VC-LS_Time_Conversion-548", 1784592, 0),
        Result("VC-LS_Time_Conversion-869", 157464, 0),
        Result("VC-LS_Time_Conversion-842", 524880, 0),
        Result("VC-LS_Time_Conversion-870", 1679616, 0),
        Result("VC-LS_Time_Conversion-872", 892296, 0),
        Result("VC-LS_Time_Conversion-873", 1994544, 0)
    ]
    res = parse_results([Path(r"../input/ls_time_conversion/long_str.ctr").resolve()])
    assert res == answer


def test_failed_results():
    answer = [
        Result("VC-LS_Time_Conversion-550", 2, 1),
        Result("VC-LS_Time_Conversion-548", 31, 3),
        Result("VC-LS_Time_Conversion-869", 0, 3),
        Result("VC-LS_Time_Conversion-842", 8, 2),
        Result("VC-LS_Time_Conversion-870", 25, 7),
        Result("VC-LS_Time_Conversion-872", 12, 5),
        Result("VC-LS_Time_Conversion-873", 30, 8)
    ]
    res = parse_results([Path(r"../input/ls_time_conversion/failed_str.ctr").resolve()])
    assert res == answer


###############################################
####     Database Configuration TESTING    ####
###############################################

def test_db_config():
    answer = {"url": "localhost"}
    res = parse_config(Path(r"../input/ls_time_conversion/db_con_test.toml").resolve())
    assert res == answer


####################################
####   Make Payload TESTING     ####
####################################


def test_create_payloads():

    file1 = File("fil1", 55.55, 55.55, 55.55, 55.55,["CSU1", "CSU2"])
    file2 = File("fil2", 44.44, 44.44, 44.44, 44.44, ["CSU2", "CSU3"])
    file3 = File("fil3", 33.33, 33.33, 33.33, 33.33)
    sent_CSCI = 1
    files = [file1, file2, file3]
    test_run_id = 1

    CSU_payload, file_payload, test_result_payload = create_payloads_without_matched_id(sent_CSCI, files, test_run_id)

    assert CSU_payload == [{'name': 'CSU1', 'csci': 1}, {'name': 'CSU2', 'csci': 1}, {'name': 'CSU3', 'csci': 1}, {'name': 'General', 'csci': 1}]

    assert file_payload == [{'name': 'fil1', 'csus': ['CSU1', 'CSU2']}, {'name': 'fil2', 'csus': ['CSU2', 'CSU3']}, {'name': 'fil3', 'csus': ['General']}]

    assert test_result_payload == [{'file': {'name': 'fil1', 'csus': ['CSU1', 'CSU2']}, 'test_run': 1, 'HLR_entry': 55.55, 'statement_coverage': 55.55,
                                    'decision_coverage': 55.55, 'MC_DC_coverage': 55.55}, {'file': {'name': 'fil2', 'csus': ['CSU2', 'CSU3']},
                                    'test_run': 1, 'HLR_entry': 44.44, 'statement_coverage': 44.44, 'decision_coverage': 44.44, 'MC_DC_coverage': 44.44},
                                    {'file': {'name': 'fil3', 'csus': ['General']}, 'test_run': 1, 'HLR_entry': 33.33, 'statement_coverage': 33.33,
                                    'decision_coverage': 33.33, 'MC_DC_coverage': 33.33}]


def test_csu_id_match():
    file_payload = [{'name': 'fil1', 'csus': ['CSU1', 'CSU2']}, {'name': 'fil2', 'csus': ['CSU2', 'CSU3']}, {'name': 'fil3', 'csus': ['General']}]

    csus = [{"id":1,"name":"General", "csci":1 }, {"id":2, "name":"CSU1", "csci":1}, {"id":3, "name":"CSU2", "csci":1}, {"id":4, "name":"CSU3", "csci":1}]

    file_payload = match_file_with_csu_id(file_payload, csus)

    assert file_payload == [{'name': 'fil1', 'csus': [2, 3]}, {'name': 'fil2', 'csus': [3, 4]}, {'name': 'fil3', 'csus': [1]}]


def test_result_id_match():
    file1 = {"id":1, "name":"fil1", "HLR_entry":55.55, "statement_coverage":55.55, "decision_coverage":55.55, "MC_DC_coverage":55.55,"csus":[2, 3]}
    file2 = {"id":2, "name":"fil2", "HLR_entry":44.44, "statement_coverage":44.44, "decision_coverage":44.44, "MC_DC_coverage":44.44,"csus":[3, 4]}
    file3 = {"id":3, "name":"fil3", "HLR_entry":33.33, "statement_coverage":33.33, "decision_coverage":33.33, "MC_DC_coverage":33.33, "csus":[1]}

    files = [file1, file2, file3]
    test_result_payload = [{'file': {'name': 'fil1', 'csus': ['CSU1', 'CSU2']}, 'test_run': 1, 'HLR_entry': 55.55, 'statement_coverage': 55.55,
                                        'decision_coverage': 55.55, 'MC_DC_coverage': 55.55}, {'file': {'name': 'fil2', 'csus': ['CSU2', 'CSU3']},
                                        'test_run': 1, 'HLR_entry': 44.44, 'statement_coverage': 44.44, 'decision_coverage': 44.44, 'MC_DC_coverage': 44.44},
                                        {'file': {'name': 'fil3', 'csus': ['General']}, 'test_run': 1, 'HLR_entry': 33.33, 'statement_coverage': 33.33,
                                        'decision_coverage': 33.33, 'MC_DC_coverage': 33.33}]
    csus = [{"id":1,"name":"General", "csci":1 }, {"id":2, "name":"CSU1", "csci":1}, {"id":3, "name":"CSU2", "csci":1}, {"id":4, "name":"CSU3", "csci":1}]


    test_result_payload = match_test_result_with_file_id(files, test_result_payload, csus)

    assert test_result_payload == [{'file': 1, 'test_run': 1, 'HLR_entry': 55.55, 'statement_coverage': 55.55,
                                    'decision_coverage': 55.55, 'MC_DC_coverage': 55.55}, {'file': 2, 'test_run': 1,
                                    'HLR_entry': 44.44, 'statement_coverage': 44.44, 'decision_coverage': 44.44,
                                    'MC_DC_coverage': 44.44}, {'file': 3, 'test_run': 1, 'HLR_entry': 33.33, 'statement_coverage': 33.33,
                                    'decision_coverage': 33.33, 'MC_DC_coverage': 33.33}]

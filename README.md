# TCV tests
This submodule is the container for all testing files that are used in testing TCV.  
Pushing changes to this repository will update the .gitmodules in the parent repository, TCV.  
To allow other users to pull your changes this updated .gitmodules needs to be pushed aswell, in the parent repository.  
The instructions for using this submodule are given in the README of the parent repository.  
All commands with starting with git submodule are *not* used here.